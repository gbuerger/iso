## usage:  CEOF = ceof (x, neof = 0, jo = 350, IC, IV, IN)
## 
## Fits an AR-model to x and computes the (C)POPs
##
## x   =  signal to estimate
## neof =  threshold cum. EV
## IC, IV calibration and validation periods
function CEOF = ceof (X, neof = 0, jo = 350, IC, IV, IN)

   global jCAL
   
   [N, M] = size(X) ;

   if (nargin < 3)
      IC = IV = true(N,1) ;
   endif

   X = X - nanmean(X(IC,:)) ;

   if exist(efile = sprintf("data/BSISO/ceof.%d.ob", jCAL), "file") == 2
      printf("<-- %s\n", efile) ;
      load(efile) ;
   else
      CX = mat2cell(X, N, ones(M, 1)) ;
      Z = cell2mat(parfun(@(c) hilbert(c), CX, "UniformOutput", false)) ;
      clear X CX ;
      [E PC ev lmb] = pca(Z(IC,:), 1, false) ;
      PC = Z * E ;
      printf("--> %s\n", efile) ;
      save(efile, "Z", "E", "PC", "ev", "lmb") ;
   endif

   if exist(Efile = sprintf("data/BSISO/Ceof.%d.ob", jCAL), "file") == 2
      load(Efile) ;
   else
      J = [1:5:40 50:50:1000] ; clear CR CI RR RI ;
      Zc = @(j) CHT_trunc(PC(IC,1:j), Z, E(:,1:j)) ;
      [R.C C.C] = parfun(@(j) re(Z(IC,:)(:), Zc(j)(IC,:)(:)), J) ;
      [R.V C.V] = parfun(@(j) re(Z(IV,:)(:), Zc(j)(IV,:)(:)), J) ;
      [HT.R HT.C] = parfun(@(j) re(Z(IV,:)(:), (PC(:,1:j) * E(:,1:j)')(IV,:)(:)), J) ;
      save(Efile, "J", "R", "C", "HT") ;
      ##	 [RR.V CR.V] = parfun(@(j) re(real(Z(IV,:))(:), real(Zc(j)(IV,:)(:))), J) ;
      ##	 [RI.C CI.C] = parfun(@(j) re(imag(Z(IC,:))(:), imag(Zc(j)(IC,:)(:))), J) ;
      ##	 [RI.V CI.V] = parfun(@(j) re(imag(Z(IV,:))(:), imag(Zc(j)(IV,:)(:))), J) ;
      ##	 save(Efile, "J", "RR", "RI", "CR", "CI") ;
   endif

   if exist(pfile = sprintf("nc/EV.svg"), "file") ~= 2

      set(0, "defaultaxesfontname", "Liberation Sans", "defaultaxesfontsize", 16) ;
      set(0, "defaulttextfontname", "Linux Biolinum", "defaulttextfontsize", 16, "defaultlinelinewidth", 1) ;
      set(gcf, "paperpositionmode", "auto") ;

      c0 = 0.1 ; c1 = 0.5 ; sv = 0.15 ;
      col.C = [c0 c0 c1] ; col.V = [c0 c1 c0] ;
      lw.C = 2 ; lw.V = 2 ; ls.r = "-" ; ls.i = "--" ;
      set(gcf, "position", [0.7 0.7 0.3 0.3]) ;

      clf ; hold on
      h.C = plot(J', 100*R.C', "color", col.C, "linestyle", "-", "linewidth", lw.C) ;
      h.V = plot(J', 100*R.V', "color", col.V, "linestyle", "-", "linewidth", lw.V) ;
      h.HT = plot(J', cumsum(ev)(J)', "color", 0.7*[1 1 1], "linewidth", 2) ;
      xlabel("# EOFs") ; ylabel("EV [%]") ;
      set(gca, "ylim", [0 100], "ygrid", "on", "gridalpha", 0.5)
      plot([jo jo], [0 100], "color", 0*[1 1 1], "linewidth", 2, "linestyle", "--") ;
      title("real-time approximation of full HT fields (OLR,U850)") ;
      legend([h.C h.V h.HT], {"calibration" "validation" "full HT"}, "box", "off", "location", "southeast") ;
      print(pfile) ;
      j = find(J == jo) ;
      printf("EV = %5.1f (C), %5.1f (V)\n", 100*[R.C(j) R.V(j)]) ;
      
#{
      subaxis(2, 1, 1, "sv", sv) ;
      cla ; hold on
      [ax.C hC.C hR.C] = plotyy(J', abs(C.C)', J', 100*R.C') ;
      plot(ax.C(2), J', cumsum(ev)(J)', "color", 0.7*[1 1 1], "linewidth", 0.5) ;
      xlabel("# EOFs") ;
      ylabel(ax.C(1), "{\\it\\rho}") ;
      ylabel(ax.C(2), "EV [%]") ;
      set(ax.C, {"ycolor"}, {col.C; col.R}) ;
      ##	 hCI.C = plot(ax.C(1), J', CI.C') ;
      ##	 hRI.C = plot(ax.C(2), J', 100*RI.C') ;
      line(ax.C(1), [jo jo], [0 1], "color", 0.7*[1 1 1], "linewidth", 3) ;
      ht = title("causal HT approximation of full HT fields (OLR,U850)\ncalibration") ;
      hl.C = legend([hC.C hR.C], {"|{\\it\\rho}|" "EV"}, "box", "off", "location", "southeast") ;
      set(gca, "ygrid", "on", "gridalpha", 0.5)
      subaxis(2, 1, 2, "sv", sv) ;
      cla ; hold on
      [ax.V hC.V hR.V] = plotyy(J', C.V', J', 100*R.V') ;
      xlabel("# EOFs") ;
      ylabel(ax.V(1), "{\\it\\rho}") ;
      ylabel(ax.V(2), "EV [%]") ;
      set(ax.V, {"ycolor"}, {col.C; col.R}) ;
      ##	 hCI.V = plot(ax.V(1), J', CI.V') ;
      ##	 hRI.V = plot(ax.V(2), J', 100*RI.V') ;
      set([ax.C(1) ; ax.V(1)], "ylim", [0 1]) ;
      set([ax.C(2) ; ax.V(2)], "ylim", [0 100]) ;
      set([hC.C hC.V], "color", col.C) ;
      set([hR.C hR.V], "color", col.R) ;
      set([hC.C hR.C], "linewidth", lw.C) ;
      set([hC.V hR.V], "linewidth", lw.V) ;
      ##	 set([hCR.C hCI.C hRR.C hRI.C], "linewidth", lw.C) ;
      ##	 set([hCR.V hCI.V hRR.V hRI.V], "linewidth", lw.V) ;
      ##	 set([hCR.C hCR.V hRR.C hRR.V], "linestyle", ls.r) ;
      ##	 set([hCI.C hCI.V hRI.C hRI.V], "linestyle", ls.i) ;
      line(ax.V(1), [jo jo], [0 1], "color", 0.7*[1 1 1], "linewidth", 3) ;
      ht = title("validation") ;
      hl.V = legend([hC.V hR.V], {"|{\\it\\rho}|" "EV"}, "box", "off", "location", "southeast") ;
      set(gca, "ygrid", "on", "gridalpha", 0.5)
      print(pfile) ;

      printf("ev = %3.1f\n", cumsum(ev)(jo)) ;
      printf("EV:\nr(C)\ti(C)\tr(V)\ti(V)\n%6.0f%%\t%6.0f%%\n", 100*[R.C ; R.V](:,find(J == jo))) ;
      printf("RR:\nr(C)\ti(C)\tr(V)\ti(V)\n%6.2f\t%6.2f\n", [C.C ; C.V](:,find(J == jo))) ;
      ## for better reconstructing CC
      ##      Ec = pca(CC(IC,:) * E(:,1:jo)', -neof) ;
      ##      save(Efile, "J", "jo", "CC", "Ec", "Zc", "RR", "RI", "CR", "CI") ;
#}
      
   endif

   [Zc CC] = CHT_trunc(PC(IC,1:jo), Z, E(:,1:jo)) ;
   RC = corr(imag(Z(IC,:)(:)), imag(Zc(IC,:)(:))) ;
   RV = corr(imag(Z(IV,:)(:)), imag(Zc(IV,:)(:))) ;
   rC = corr(imag(PC(IC,1:jo))(:), imag(CC(IC,:))(:)) ;
   rV = corr(imag(PC(IV,1:jo))(:), imag(CC(IV,:))(:)) ;
   printf("%5.2f%%: %5.2f\t%5.2f\t%5.2f\t%5.2f\n", sum(ev(1:jo)), rC, rV, RC, RV)

##   df = Inf ; tol = 1e-4 ;
##   while df > tol
##      S = AR(CC(IC,:)) ;
##      cc = chilbert(S, real(CC)) ;
##      z = cc * E(:,1:jo)' ;
##      df = sumsq(cc(:) - CC(:)) / sumsq(CC(:)) ;
##      RC = corr(X(IC,:)(:), real(z(IC,:)(:))) ;
##      RV = corr(X(IV,:)(:), real(z(IV,:)(:))) ;
##      rC = corr(imag(PC(IC,:))(:), imag(CC(IC,:))(:)) ;
##      rV = corr(imag(PC(IV,:))(:), imag(CC(IV,:))(:)) ;
##      printf("%d\t%5.2f\t%5.2f\t%5.2f\t%5.2f\t%10.6f\n", j, rC, rV, RC, RV, df)
##      fflush(stdout) ;
##      CC = cc ;
##   endwhile

#{
   ys = std(imag(PC(IC,1:jo))) ;
   clear cc ;
   for j=1:10
      y0 = 1 * randn * ys ;
      cc(:,:,j) = chilbert(S, real(PC(:,1:jo)), y0) ;
   endfor
   q = std(imag(PC(IC,1:jo))) ./ std(imag(CC(IC,1:jo))) ;

   [R lag ix] = xcor(imag(PC(:,1)), imag(CC(:,1)), 20) ;

   addpath ~/oct/nc/popout
   n = 500 ;
   clf ; hold on ;
   hg = hggroup ;
   plot(zscore(squeeze(imag(cc(1:n,1,:)))), "color", 0.7 * [1 1 1], "linewidth", 1, "parent", hg) ;
   Ic = find(IV)(1:n) ;
##   plot(zscore(imag(PC(Ic,1))), "k", "linewidth", 3)
   xlabel("time  [d]") ; ylabel("\\it\\eta^i") ;
   zh = CC * E(:,1:jo)' ;
   printf("%d\t%5.2f\t%5.2f\n", jo, corr(X(IC,:)(:), real(zh(IC,:)(:))), corr(X(IV,:)(:), real(Zh(IV,:)(:))))
   props.axes1.position = [0.1 0.53 0.8 0.45];
   props.axes2.position = [0.3 0.08 0.6 0.3];
   props.axes1.fontsize = 14;
   props.axes2.fontsize = 14;
   props.axes2.box = "on";
   props.axes2.linewidth = 1;
   props.axis1.xlabel = 'time  [d]';
   props.axis1.ylabel = '\\it{\\xi^i}';
   props.axis2.ylabel = '';
   ##	 props.axis1.xlabel = 'closeup';
   [ax1 ax2] = popout(gcf, 0, 70, props);
##   hl = legend(ax2, {"{\\bfC}EOF" "CEOF"}, "box", "off", "location", "north") ;
   hgsave("data/BSISO/Ceof.og") ;

   print("nc/Ceof.svg") ;

   xl = [min(imag([PC CC])(:)) max(imag([PC CC])(:))] ;
   clf ;
   ax(1) = subplot(1,2,1) ;
   plot(imag(PC(IC,:))(:), imag(CC(IC,:))(:), "k.") ;
   xlabel("\\it\\xi^i") ; ylabel("\\it\\xi_c^i") ;
   title("calibration (1981–1995)") ;
   text(-50, 50, sprintf("\\it\\rho = %5.2f", corr(imag(PC(IC,:))(:), imag(CC(IC,:))(:)))) ;
   ax(2) = subplot(1,2,2) ;
   plot(imag(PC(IV,:))(:), imag(CC(IV,:))(:), "k.") ;
   xlabel("\\it\\xi^i") ; ylabel("\\it\\xi_c^i") ;
   title("validation (1996–2010)") ;
   set(ax, "xlim", xl, "ylim", xl, "xaxislocation", "origin", "yaxislocation", "origin", "plotboxaspectratio", [1 1 1], "box", "off") ;
   text(-50, 50, sprintf("\\it\\rho = %5.2f", corr(imag(PC(IV,:))(:), imag(CC(IV,:))(:)))) ;

   set(gcf, "paperpositionmode", "auto")
   print("nc/PC-CC.png") ;
   print("nc/PC-CC.svg") ;
#}

   if neof > 0
      E = E(:,1:neof) ;
      PC = PC(:,1:neof) ;
      ev = ev(1:neof) ;
      lmb = lmb(1:neof) ;
      CC = CC(:,1:neof) ;
   else
      E = eye(M) ;
      PC = X ;
   endif

   pat = nan(size(E)) ;
   I = all(isfinite(E), 2) ;
   pat(I,:) = E(I,:) ;

   ## sort Eigenvalues
   [~, is] = sort(abs(lmb), "descend") ;
   pat = pat(:,is) ;
   pc = PC(:,is) ;
   cc = CC(:,is) ;
   lmb = lmb(is) ;

   amp = -1./log(abs(lmb)) ;
   per = 2*pi ./ arg(lmb) ;

   CEOF = struct("pat", pat, "pc", pc, "cc", cc, "lmb", lmb, "amp", amp, "per", per) ;

   ## explained variance
   [CEOF.ev CEOF.er] = parfun(@(j) re_flt(real(pc(IV,j) * pat'(j,IN))(:), X(IV,IN)(:)), 1:columns(pat)) ;
   pev = parfun(@(i) arrayfun(@(j) re_flt(real(pc(IV,j) * pat'(j,i)), X(IV,i)), 1:columns(pat))', 1:M, "UniformOutput", false) ;
   CEOF.pev = cell2mat(pev)' ;

##   CEOF.cv1 = parfun(@(j) re(real(cc(IV,j) * Ec'(j,:))(:), X(IV,IN)(:)), 1:columns(pat))' ;
##   cev = parfun(@(i) arrayfun(@(j) re(real(cc(IV,j) * Ec'(j,i)), X(IV,i)), 1:columns(pat))', 1:M, "UniformOutput", false) ;
##   CEOF.cev1 = cell2mat(cev)' ;

   [CEOF.cv CEOF.cr] = parfun(@(j) re_flt(real(cc(IV,j) * pat'(j,IN))(:), X(IV,IN)(:)), 1:columns(pat)) ;
   cev = parfun(@(i) arrayfun(@(j) re_flt(real(cc(IV,j) * pat'(j,i)), X(IV,i)), 1:columns(pat))', 1:M, "UniformOutput", false) ;
   CEOF.cev = cell2mat(cev)' ;

endfunction


## usage: [Zc CC] = CHT_trunc (PC, Z, E)
##
## calculate truncated CHT
function [Zc CC] = CHT_trunc (PC, Z, E)

   S = AR(PC) ;
   clear PC ;
   pc = Z * E ;
   clear Z ;
   x = real(pc) ;
   CC = chilbert(S, x) ;
   
   if 0
      F = @(y) cost_CHT(S, x, y) ;
      opts = optimset("Display", "Iter") ;
      CC = fminsearch(F, imag(CC)) ;
   endif

   clear pc S ;
   Zc = CC * E' ;

endfunction


## usage: f = cost_CHT (S, x, y)
##
##
function f = cost_CHT (S, x, y)

   y = reshape(y, size(x)) ;
   z = complex(x, y) ;

   f = sumsq(shift(z, -1, 1) - S * z(1:end-1,:)) ;
   
endfunction

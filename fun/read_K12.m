## usage: [K12 rK12] = read_K12 (file, rfile, MON)
##
## read K12, select MON
function [K12 rK12] = read_K12 (file, rfile, MON)

   w = dlmread(file) ;
   wr = dlmread(rfile) ;

   w = w(2:end,:) ;
   wr = wr(2:end,:) ;
   
   K12.id = w(:,1:3) ;
   K12.x = w(:,4:5) ;
   rK12.id = wr(:,1:3) ;
   rK12.x = wr(:,4:5) ;

   K12.IM = ismember(K12.id(:,2), MON) ;
   rK12.IM = ismember(rK12.id(:,2), MON) ;

endfunction

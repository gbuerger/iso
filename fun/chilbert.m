## usage: z = chilbert (S, x, y0 = 0, iREC = 1)
##
## perform causal Hilbert transform using system matrix S
function z = chilbert (S, x, y0 = 0)

   global CHT_FULL
   
   Sr = real(S) ;
   Si = imag(S) ;

   y = nan(size(x)) ;

   if CHT_FULL

      SiI = inv(Si) ;
      A1 = Sr * SiI * Sr + Si ;
      A0 = Sr * SiI ;

      x0 = x(1,:) - (x(2,:) - x(1,:)) ;
      xs = [x0 ; x] ;
      y = cell2mat(arrayfun(@(i) xs(i-1,:) * A1 - xs(i,:) * A0, 2:rows(xs), "UniformOutput", false)') ;
      
   else

      y(1,:) = y0 ;   
      for i = 2 : rows(x)
	 y(i,:) = y(i-1,:) * Sr + x(i-1,:) * Si ;
      endfor

   endif
   
   z = complex(x, y) ;
   
endfunction

## usage: y = nrm (x, I)
##
## normalize x wrt. I
function y = nrm (x, I)

   xm = nanmean(x(I,:)) ;
   xs = nanstd(x(I,:)) ;

   y = anom(x, xm, xs) ;
   
endfunction

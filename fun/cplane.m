## usage: [phi, ev, sev] = cplane (C, jC, x)
##
## determine best phi in cplane, with scores
function [phi, ev, sev] = cplane (C, jC, x)

   nx = columns(x) ;
   cr = crot(C(:,jC), 0)' ;

   PHI = linspace(0, 2*pi, 100) ;

   j = 0 ;
   for phi = PHI

      j++ ;
      
      ci = crot(C(:,jC), phi)' ;
      S = [cr ; ci] ;
      xi = x / S ;

      xhat = xi * S ;

      ev(j) = rv(xhat(:), x(:)) ;
##      sev(j,:) = cell2mat(parfun(@(i) rv(xhat(:,i), x(:,i)), 1:columns(x), "UniformOutput", false)) ;

   endfor
   
endfunction

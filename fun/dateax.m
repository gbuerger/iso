function res = dateax (id)

   ## usage: res = dateax (id)
   ## 
   ## transform date ids to datenums

   N = size(id) ;

   if N(2) < 2
     id = [id ones(N(1), 2)] ;
   endif
   if N(2) < 3
     id = [id ones(N(1), 1)] ;
   endif

   res = nan(N(1), 1) ;

   I = all(~isnan(id), 2) ;

   res(I) = datenum(id(I,:)) ;

endfunction

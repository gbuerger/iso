## usage: [h hq] = phsplot (phs, amp, I, col, varargin)
##
## phase plot for phs, amp
function [h hq] = phsplot (phs, amp, I, col, varargin)

##   h = mmpolar(phs(I), amp(I), varargin{:}) ;
##   h = polar(phs(I), amp(I), varargin{:}) ;
   h = plot(phs(I), amp(I), varargin{:}) ;

   set(h, "color", col) ;

   if nargout > 1
   
      hold on ;
      [xt, yt, u, v] = addarr(phs(I), amp(I)) ;
      hq = quiver(xt, yt, u./rms([u ; v]), v./rms([u ; v]), 0.2);
      set(allchild(hq)(2), "color", col) ;
      ##   set(allchild(hq)(3), "linestyle", "none") ;

   endif
   
endfunction

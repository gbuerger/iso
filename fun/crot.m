## usage: x = crot (z, phi)
##
## rotate z in its real plane by phi (radians)
function x = crot (z, phi)

   x = real(exp(i*phi) * z) ;

endfunction

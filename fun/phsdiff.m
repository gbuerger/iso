## usage: d = phsdiff (x, y, q = 0.7)
##
##
function d = phsdiff (x, y, q = 0.7)

   d = x - y ;

   I = d < -q*2*pi ;
   d(I) += 2*pi ;

   I = d > q*2*pi ;
   d(I) -= 2*pi ;
   
endfunction

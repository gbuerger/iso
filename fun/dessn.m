## usage: varargout = dessn (u, ps)
##
## de-seasonalize u
function varargout = dessn (u, ps)

   N = size(u.x) ;
   u.x = reshape(u.x, N(1), prod(N(2:end))) ;

   if nargin > 1
      ps = reshape(ps, rows(ps), prod(N(2:end))) ;
      u = anncyc(u, ps) ;
      u.ps = reshape(ps, [rows(ps) N(2:end)]) ;
      u.x = reshape(u.x, N) ;
      u.a = reshape(u.a, N) ;
      varargout{1} = u ;
   else
      if nargout < 2
	 w = anncyc(u) ;
	 varargout{1} = reshape(w, [rows(w) N(2:end)]) ;
      else
	 [ps w] = anncyc(u) ;
	 ps = reshape(ps, [rows(ps) N(2:end)]) ;
	 w.x = reshape(w.x, N) ;
	 w.a = reshape(w.a, N) ;
	 varargout{1} = ps ;
	 varargout{2} = w.a ;
      endif
   endif

   return ;
   
   pkg load signal

   N = size(v.x) ;
   v.xm = nanmean(v.x, 1) ;
   w = reshape(v.x, N(1), prod(N(2:end))) ;
   wm = reshape(v.xm, 1, prod(N(2:end))) ;

   I = find(any(any(isnan(w), 2), 3)) ;
   for i = I'
      w(i,:) = wm ;
   endfor

   if 1						# use BW
      [sc.b sc.a] = butter(3, 1./SC, "stop") ; # first three harmonics
      [sm.b sm.a] = butter(3, 1./SM, "high") ;
   else
      sc.b = fir1(30, 1./SC, "stop") ; sc.a = 1 ; # first three harmonics
      sm.b = fir1(30, 1./SM) ; sm.a = 1 ;
   endif

   wa = filtfilt(sc.b, sc.a, w - wm) ;
   wf = filtfilt(sm.b, sm.a, wa) ;

   v.xa = reshape(wa, N) ;
   v.xf = reshape(wf, N) ;
   
endfunction

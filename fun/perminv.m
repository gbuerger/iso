function I = perminv (s1, s2)

   ## usage:  I = perminv (s1, s2)
   ##
   ## map cell s1 to s2, so that s1(I) = s2

   if nargin < 2
      s2 = 1:length(s1) ;
   endif

   [w p1] = sort(s1) ;
   [w p2] = sort(s2) ;
   [w sp2] = sort(p2) ;

   I = p1(sp2) ;

endfunction

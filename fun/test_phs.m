## usage: h = test_phs (S, y, m, p, s)
##
## test phase
function h = test_phs (S, y, m, p, s)


   S.x = [s(1) * S.x(:,p(1)) s(2) * S.x(:,p(2))] ;

   clf ; hold on

   z = 0.5 ;
   col = {[z z 1] [1 z 0] [z 1 z]} ; lw = 1.5 ;
   
   s = S ;
   s.I = sdate(S.id, [y m 1 ; y m 31]) ;
   h(1) = phsplot(s.x(:,1), s.x(:,2), s.I, col{1}, "linewidth", lw(1)) ;
   scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, col{1}, "filled") ;

   s = S ;
   s.I = sdate(S.id, [y m+1 1 ; y m+1 31]) ;
   h(2) = phsplot(s.x(:,1), s.x(:,2), s.I, col{2}, "linewidth", lw(1)) ;
   scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, col{2}, "filled") ;

   s = S ;
   s.I = sdate(S.id, [y m+2 1 ; y m+2 31]) ;
   h(3) = phsplot(s.x(:,1), s.x(:,2), s.I, col{3}, "linewidth", lw(1)) ;
   scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, col{3}, "filled") ;

   set(gca, "Xaxislocation", "origin", "Yaxislocation", "origin")

   grid on
   axis([-3 3 -3 3]) ;
   axis square

   legend(h, {"Oct" "Nov" "Dec"}, "box", "off", "location", "northeastoutside")

endfunction

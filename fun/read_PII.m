## usage: [PII rPII] = read_PII (ncf, MON)
##
## read PII, select MON
function [PII rPII] = read_PII (ncf, MON)

   pkg load netcdf

   t0 = datenum(1998, 1, 1) ;
   t = t0 + ncread(ncf, "time") ;

   PII.id = rPII.id = datevec(double(t))(:,1:3) ;
   PII.x = ncread(ncf, "mjo_ind")' ;
   rPII.x = ncread(ncf, "mjo_rt")' ;
   PII.lon = rPII.lon = ncread(ncf, "lon") ;
   PII.lat = rPII.lat = ncread(ncf, "lat") ;
   PII.eof = rPII.eof = ncread(ncf, "eof") ;

   PII.IM = ismember(PII.id(:,2), MON) ;
   rPII.IM = ismember(rPII.id(:,2), MON) ;

endfunction

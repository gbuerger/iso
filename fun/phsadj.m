## usage: res = phsadj (phs, th)
##
## adjust phase speed with threshold th
function res = phsadj (phs, th = 0.8)

   res = phs(:) ;

   for j = 1:rows(res)-1

      if res(j+1) - res(j) < -th*pi
	 res(j+1:end) += 2*pi ;
      endif

      if res(j+1) - res(j) > th*pi
	 res(j+1:end) -= 2*pi ;
      endif

   endfor

   res = reshape(res, size(phs)) ;
   
endfunction

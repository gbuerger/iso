## usage: RMM = read_RMM (file, MON)
##
## read RMM, select MON
function RMM = read_RMM (file, MON)

   w = dlmread(file) ;

   RMM.id = w(3:end,1:3) ;
   RMM.x = w(3:end,4:5) ;
   RMM.phs = w(3:end,6) ;
   RMM.amp = w(3:end,7) ;

   RMM.IM = ismember(RMM.id(:,2), MON) ;
   
endfunction

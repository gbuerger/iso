## usage: [xt yt u v] = addarr (the, amp)
##
## add arrows to polar plot
function [xt yt u v] = addarr (the, amp)

   [X,Y] = pol2cart(the, amp); % get Cartesian coordinates
   factor = 5; % sampling ratio; factor=5 means the arrow will be drawn for every 5th point on the curve
   i = 1; % index into quiver array
   j = factor * factor; % index into X,Y Cartesian coordinates
   u = []; % array for x component of quiver vector
   v = []; % array for y component of quiver vector
   xt = []; % array for x position of quiver vector
   yt = []; % array for y position of quiver vector
   while (j+1) <= length(X)
      xt(i) = X(j);
      yt(i) = Y(j);
      u(i) = X(j+1) - X(j);
      v(i) = Y(j+1) - Y(j);
      i = i + 1;
      j = i*factor;
   end

endfunction

function [y ym ys] = anom (x, xm, xs)

   ## usage:  [y ym ys] = anom(x, xm, xs)
   ## anomalies of x rel. to xm, xs

   pkg load statistics
   
   global REC DLEN

   if isempty(REC), REC = 1 ; endif

   p = [REC 1:REC-1 REC+1:ndims(x)] ;
   x = permute(x, p) ;

   nr = rows(x) ;

   if (nargin > 1)
      ym = xm ;
   else
      ym = nanmean(x) ;
   endif 

   y = x - repmat(ym, nr, 1) ;

   if (nargin > 2 || nargout > 2)
      if (nargin > 2)
	 ys = xs ;
      else
	 ys = nanstd(x) ;
      endif
      y = y ./ repmat(ys, nr, 1) ;
   endif 

   y = permute(y, perminv(p)) ;

endfunction

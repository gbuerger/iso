## usage: [OMI rOMI] = read_OMI (file, rfile, MON)
##
## read OMI, select MON
function [OMI rOMI] = read_OMI (file, rfile, MON)

   w = dlmread(file) ;
   wr = dlmread(rfile) ;

   OMI.id = w(:,1:3) ;
   OMI.x = w(:,5:6) ;
   rOMI.id = wr(:,1:3) ;
   rOMI.x = wr(:,5:6) ;

   OMI.IM = ismember(OMI.id(:,2), MON) ;
   rOMI.IM = ismember(rOMI.id(:,2), MON) ;
   
endfunction

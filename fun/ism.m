## usage: y = ism (x, lon, lat)
##
## calculate ism from field x
function y = ism (x, lon, lat)

   global ISM
   
   Jlon = ISM.lon(1) <= lon & lon <= ISM.lon(2) ;
   Jlat = ISM.lat(1) <= lat & lat <= ISM.lat(2) ;
   J = find(Jlon * Jlat') ;

   y = nanmean(x(:,J), 2) ;

endfunction

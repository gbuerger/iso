
clear ;
pkg load signal statistics
cd ~/iso
addpath ~/iso/fun ~/xds/fun
## ~/oct/nc/pcatool ~/oct/nc

global PAR NCPU ISM alpha = 0.05 jCAL
PAR.dbg = false ;
PAR.par_opt = {"UniformOutput", false, "VerboseLevel", 0} ; 
PAR.anc = "a" ;
PAR.numhar = 7 ;
PAR.acyc = @(t) [ones(rows(t),1) cos(t) sin(t) cos(2*t) sin(2*t) cos(3*t) sin(3*t)] ;
vangle = @(x,y) acos(dot(x, y) / (norm(x) * norm(y)))  / (2*pi) * 360 ;
rCEOFn = "rCEOF" ;

WB = true ;
MON = 5:10 ;
VAR = {"olr" "uwnd"} ;
TLAG = 120 ;
jCAL = 1 ;
TIM = {[1979 2019] [1981 2010] [1981 2019]}{jCAL} ;
CAL = {[1979 1999] [1981 1995] [1981 2019]}{jCAL} ;
LON = [40 160] ; LAT = [-10 40] ; LEV = [850 850] ;
ISMI.LON = [40 80 ; 70 90] ; ISMI.LAT = [5 15 ; 20 30] ;
WNPSMI.LON = [100 130 ; 110 140] ; WNPSMI.LAT = [5 15 ; 20 30] ;
SC = [365 365/3] ; SM = [90 25] ;
ISM.lon = [70.5 85.5] ; ISM.lat = [10.5 25.5] ;
set(0, "defaultfigureunits", "normalized", "defaultfigurepaperunits", "normalized") ;
set(0, "defaultfigureposition", [0.7 0.7 0.3 0.3], "defaultfigurepaperpositionmode", "auto") ;
set(0, "defaultaxesgridalpha", 0.3)
set(0, "defaultlinelinewidth", 2)
set(0, "defaultaxesfontsize", 18, "defaulttextfontsize", 18)
set(0, "defaultaxesfontname", "Linux Biolinum", "defaulttextfontname", "Linux Biolinum") ;

nd = sum(eomday(1900, MON)) ;

if exist(file = sprintf("data/BSISO/olr.%d.ob", jCAL), "file") == 2

   load(file) ;

else

   pkg load netcdf
   ncf = "data/olr.day.mean.nc" ;
   t0 = datenum(1800, 1, 1) ;
   t = double(ncread(ncf, "time")) / 24 ;

   tim = datevec(t0+t)(:,1) ;
   lon = double(ncread(ncf, "lon")) ;
   lat = double(ncread(ncf, "lat")) ;
   Itim = find(TIM(1) <= tim & tim <= TIM(2)) ;
   Ilon = find(LON(1) <= lon & lon <= LON(2)) ;
   Ilat = find(LAT(1) <= lat & lat <= LAT(2)) ;
   olr.id = datevec(t0 + t(Itim)) ;
   olr.lon = lon(Ilon) ;
   olr.lat = lat(Ilat) ;

   start = [Ilon(1) Ilat(1) Itim(1)] ;
   count = [length(Ilon) length(Ilat) length(Itim)] ;
   olr.x = double(ncread(ncf, "olr", start, count)) ;

   olr.x = permute(olr.x, [3 1 2]) ;
   olr.N = size(olr.x) ;
   save(file, "olr") ;

endif


if exist(file = sprintf("data/BSISO/uwnd.%d.ob", jCAL), "file") == 2

   load(file) ;

else

   y = TIM(1) ;
   ncf = sprintf("http://www.esrl.noaa.gov/psd/thredds/dodsC/Datasets/ncep.reanalysis2.dailyavgs/pressure/uwnd.%d.nc", y) ;
   t0 = datenum(1800, 1, 1) ;
   t = double(ncread(ncf, "time")) / 24 ;
   tim = datevec(t0+t)(:,1) ;
   lon = double(ncread(ncf, "lon")) ;
   lat = double(ncread(ncf, "lat")) ;
   lev = double(ncread(ncf, "level")) ;
   Ilon = find(LON(1) <= lon & lon <= LON(2)) ;
   Ilat = find(LAT(1) <= lat & lat <= LAT(2)) ;
   Ilev = find(LEV(1) <= lev & lev <= LEV(2)) ;
   uwnd.lon = lon(Ilon) ;
   uwnd.lat = lat(Ilat) ;
   start = [Ilon(1) Ilat(1) Ilev(1) 1] ;
   count = [length(Ilon) length(Ilat) length(Ilev) Inf] ;

   Y = TIM(1):TIM(end) ;
   if WB hw = waitbar(0, "looping through uwnd") ; endif
   i = 0 ; uwnd.id = uwnd.x = [] ;
   for y = Y
      
      i++ ;
      ncf = sprintf("http://www.esrl.noaa.gov/psd/thredds/dodsC/Datasets/ncep.reanalysis2.dailyavgs/pressure/uwnd.%d.nc", y) ;
      t = double(ncread(ncf, "time")) / 24 ;
      s.id = datevec(t0 + t) ;
      s.x = double(ncread(ncf, "uwnd", start, count)) ;
      s.x = squeeze(permute(s.x, [4 1 2 3])) ;
      uwnd.id = [uwnd.id ; s.id(:,1:3)] ;
      uwnd.x = [uwnd.x ; s.x] ;
      waitbar(i / length(Y), hw) ;
      
   endfor
   if WB close(hw) ; endif
   uwnd.N = size(uwnd.x) ;
   save(file, "uwnd") ;

endif

## seasonal cycle and smoothing
if exist(afile = sprintf("data/BSISO/all.%d.ob", jCAL), "file") == 2
   load(afile) ;
else
   IM = ismember(olr.id(:,2), MON) ;
   IY = ismember(olr.id(:,1), CAL(1):CAL(2)) ;
   IC = IY & IM ;
   for v = VAR
      v = v{:} ;
      eval(sprintf("[%s.ps %s.a] = dessn(%s) ;", v, v, v)) ;
      eval(sprintf("%s.f = %s.a - movmean(%s.a, [TLAG-1 0]) ;", v, v, v)) ;
      eval(sprintf("%s.f = %s.f ./ std(%s.f(IC,:,:)(:)) ;", v, v, v)) ;
   endfor
   save(afile, VAR{:}, "IM", "IY") ;
endif

## MVEOF and CPOPs
##method = "OMD" ;
method = "PC" ;
if exist(bsfile = sprintf("data/BSISO/pat.%d.%s.ob", jCAL, method), "file") == 2
   load(bsfile) ;
else
   X = [reshape(olr.f, olr.N(1), prod(olr.N(2:end))) reshape(uwnd.f, olr.N(1), prod(uwnd.N(2:end)))] ;
   IM = ismember(olr.id(:,2), MON) ;

   IY = ismember(olr.id(:,1), CAL(1):CAL(2)) ;
   IYC = IY ;
   IYV = ~IYC ;
   IYC = ismember(olr.id(:,1), 1981:1995) ;
   IYV = ismember(olr.id(:,1), 1996:2010) ;
   IC = IYC & IM ;
   IV = IYV & IM ;
   global ffun
   ffun = @(x) sgolayfilt(x, 3, 5) ; ffun = [] ;
   
##   addpath ~/oct/nc/borders
##   [ind.lat ind.lon] = borders("india") ;
##   I = all(~isnan([ind.lon; ind.lat])) ;
##   [LAT LON] = meshgrid(olr.lat, olr.lon) ;
##   [IN ON] = inpolygon(LON, LAT, ind.lon(I)', ind.lat(I)') ;
##   IN = (IN | ON) ;
##   AR = repmat(IN(:)', 1, 2) ;
   AR = true(1, columns(X)) ;

   ## EOF
   [EOF.pat, ~, EOF.pev] = pca(nancenter(X(IC,:)), -4) ;
   EOF.pc = X * EOF.pat ;
   disp(EOF.ev = parfun(@(jC) 100*rv(EOF.pc(IC,jC) * EOF.pat(AR,jC)', X(IC,AR)), 1:columns(EOF.pat))) ;
   disp(EOF.ev = parfun(@(jC) 100*rv(EOF.pc(IV,jC) * EOF.pat(AR,jC)', X(IV,AR)), 1:columns(EOF.pat))) ;
   jE = 1:2 ;
   disp(100*rv(EOF.pc(IC,jE) * EOF.pat(:,jE)', X(IC,:))) ;
   disp(100*rv(EOF.pc(IV,jE) * EOF.pat(:,jE)', X(IV,:))) ;
   disp(100*rv(EOF.pc(IC,jE) * EOF.pat(AR,jE)', X(IC,AR))) ;
   disp(100*rv(EOF.pc(IV,jE) * EOF.pat(AR,jE)', X(IV,AR))) ;
##   ffun = [] ;
   disp(100*rv_flt(EOF.pc(IV,jE) * EOF.pat(AR,jE)', X(IV,AR))) ;
   EOF.pev = parfun(@(jX) rv_flt(EOF.pc(IV,jE) * EOF.pat(jX,jE)', X(IV,jX)), 1:rows(EOF.pat))' ;

##   POP, CEOF, CPOP
##   POP = pop(X, 2, IM, method) ;
##   CV = [] ;
##   for neof = (NEOF = 1 : 100 : columns(X))
##      CEOF = ceof(X, neof, IM) ;
##      CV = [CV CEOF.cv] ;
##   endfor
##   CPOP = cpop(X, 2, IM, method) ; # using 2 PCs

   global CHT_FULL = ~true
   jo = 200 ;
   CEOF = ceof(X, 2, jo, IC, IV, AR) ;
   disp(100*[CEOF.ev(:,1) CEOF.cv(:,1)]) ;
   disp([CEOF.er(:,1) CEOF.cr(:,1)]) ;

   ## spatial patterns
   figure(1) ; clf ; pos = get(gcf, "position") ;
   load("~/oct/m_map/private/m_coasts.mat") ;
   jvar = 1 ; vn = VAR{jvar} ;	    # olr

   ## EOFs (spatial)
   sE = [1 -1] ; sC = [1 1] ; # sort order ("life cycle")
   pos1 = pos ;
   pos1(4) += 0.08 ;
   figure(2, "position", pos1) ; colormap(redblue) ;
   clf ; hold on ; clear ax
   pat = reshape(EOF.pat, [olr.N(2:3) 2 size(EOF.pat, 2)]) ;

   ax(1) = subaxis(2, 2, 1, "SpacingVert", 0.2) ; hold on ;
   contourf(olr.lon, olr.lat, squeeze(sE(1)*pat(:,:,jvar,1))') ;
   ##hc = colorbar("location", "eastoutside") ;
   plot(ncst(:,1), ncst(:,2), "k")
   xlabel("longitude") ; ylabel("latitude") ;
   ##pos = get(hc, "position") ; pos(2) -= 0.1 ;
   ##set(hc, "position", pos) ;
   title("EOF_1") ;

   ax(2) = subaxis(2, 2, 2, "SpacingVert", 0.2) ; hold on ;
   contourf(olr.lon, olr.lat, squeeze(sE(2)*pat(:,:,jvar,2))') ;
   ##hc = colorbar("location", "eastoutside") ;
   plot(ncst(:,1), ncst(:,2), "k")
   xlabel("longitude") ; ylabel("latitude") ;
   title("EOF_2") ;

   ## CEOFs (spatial)
   pat = reshape(CEOF.pat, [olr.N(2:3) 2 size(CEOF.pat, 2)]) ;
   jC = 1 ;

   ax(3) = subaxis(2, 2, 3, "SpacingVert", 0.2) ; hold on ;
   contourf(olr.lon, olr.lat, squeeze(sC(1)*real(pat(:,:,jvar,jC))')) ;
   ##hc = colorbar("location", "eastoutside") ;
   plot(ncst(:,1), ncst(:,2), "k")
   xlabel("longitude") ; ylabel("latitude") ;
   ##pos = get(hc, "position") ; pos(2) -= 0.1 ;
   ##set(hc, "position", pos) ;
   title("CEOF^r") ;

   ax(4) = subaxis(2, 2, 4, "SpacingVert", 0.2) ; hold on ;
   c = exp(i * pi/2 * sign(imag(CEOF.lmb(jC)))) ;
   contourf(olr.lon, olr.lat, squeeze(sC(2)*imag(pat(:,:,jvar,jC))')) ;
   ##hc = colorbar("location", "eastoutside") ;
   plot(ncst(:,1), ncst(:,2), "k")
   xlabel("longitude") ; ylabel("latitude") ;
   title("CEOF^i") ;
   printf("angle between CEOFs: %5.1f\n", vangle(real(CEOF.pat(:,jC)), imag(CEOF.pat(:,jC)))) ;

   set(ax([2 4]), "yaxislocation", "right") ;
   colormap(redblue)
   cl1 = [min(arrayfun(@(a) get(a, "clim")(1), ax)) max(arrayfun(@(a) get(a, "clim")(2), ax))] ;
   ##set(ax, "clim", cl1) ;
   print(sprintf("nc/olr.pat.svg")) ;

   ## sign of patterns
   EOF.pat(:,1:length(sE)) = sE .* EOF.pat(:,1:length(sE)) ; EOF.pc(:,1:length(sE)) = sE .* EOF.pc(:,1:length(sE)) ;
   CEOF.pat(:,1:length(sE)) = sC .* CEOF.pat(:,1:length(sE)) ; CEOF.pc(:,1:length(sE)) = sC .* CEOF.pc(:,1:length(sE)) ;
   CEOF.cc(:,1:length(sE)) = sC .* CEOF.cc(:,1:length(sE)) ;
   
   save(bsfile, "X", "EOF", "CEOF", "IC", "IV") ;
endif

load("~/oct/m_map/private/m_coasts.mat") ;
jvar = 1 ; vn = VAR{jvar} ;	    # olr

## spatial
figure(1) ; clf ; pos = get(gcf, "position") ;
g0 = 0.7 ;
EOF.col = g0*[1 0 0] ;
CEOF.col = g0*[0 0 1] ;
rCEOF.col = g0*[0 1 0] ;

## mean and std
pos1 = pos ;
pos1(3) += 0.1 ;
figure(2, "position", pos1) ; colormap(redblue) ;
clf ;
subplot(1,2,1) ; hold on ;
contourf(olr.lon, olr.lat, squeeze(mean(olr.x(IM,:,:)))') ; colorbar("title", "OLR [W/m^2]") ;
colormap(gca, flip(redblue))
plot(ncst(:,1), ncst(:,2), "k")
subplot(1,2,2) ; hold on ;
contourf(olr.lon, olr.lat, squeeze(std(olr.x(IM,:,:)))') ; colorbar("title", "OLR [W/m^2]") ;
plot(ncst(:,1), ncst(:,2), "k")
colormap(flip(redblue))


## temporal
PY = 2010 ;
IM = ismember(olr.id(:,2), MON) ;
IY = ismember(olr.id(:,1), PY) ;
II = IY & IM ;
figure(1, "position", [0.682   0.446   0.5   0.7])
clf ; clear ax ; sh = 0.1 ; sv = 0.15 ;

ax(1) = subaxis(3,3,1:2, "SH", sh, "SV", sv) ;
w = [EOF.pc(:,1) EOF.pc(:,2)] ; w(IY & ~IM,:) = NaN ;
h = plot(dateax(olr.id(IY&IM,:)), w(IY&IM,:), {"k-" "k--"}) ; box off ;
yl = ylim ;
datetick("mmm") ;
xlabel("time") ; ylabel("EOF PCs") ;
h = legend({"PC_1" "PC_2"}, "box", "off", "location", "northwest") ;
hpos = get(h, "position") ; hpos(2) += 0.05 ; set(h, "position", hpos) ;
ax(2) = subaxis(3,3,3, "SH", sh, "SV", sv) ;
plot(w(IY&IM,1),w(IY&IM,2), "k") ; box off ;
set(gca, "xaxislocation", "origin", "yaxislocation", "origin")
xlabel("PC_1") ; ylabel("PC_2") ; xticklabels([]) ; yticklabels([]) ;
axis square ;

ax(3) = subaxis(3,3,4:5, "SH", sh, "SV", sv) ;
jC = 1 ;
w = [real(CEOF.pc(:,jC)) imag(CEOF.pc(:,jC))] ; w(IY & ~IM,:) = NaN ;
plot(dateax(olr.id(IY&IM,:)), w(IY&IM,:), {"k-" "k--"}) ; box off ;
yl = ylim ;
xlabel("time") ; ylabel("CEOF PCs") ;
datetick("mmm") ;
h = legend({"\\it\\xi^r" "\\it\\xi^i"}, "box", "off", "location", "northwest") ;
hpos = get(h, "position") ; hpos(2) += 0.05 ; set(h, "position", hpos) ;
ax(4) = subaxis(3,3,6, "SH", sh, "SV", sv) ;
plot(w(IY&IM,1),w(IY&IM,2), "k") ; box off ;
set(gca, "xaxislocation", "origin", "yaxislocation", "origin")
xlabel("\\it\\xi^r") ; ylabel("\\it\\xi^i") ; xticklabels([]) ; yticklabels([]) ;
axis square ;

ax(5) = subaxis(3,3,7:8, "SH", sh, "SV", sv) ;
jC = 1 ;
w = [real(CEOF.cc(:,jC)) imag(CEOF.cc(:,jC))] ; w(IY & ~IM,:) = NaN ;
plot(dateax(olr.id(IY&IM,:)), w(IY&IM,:), {"k-" "k--"}) ; box off ;
yl = ylim ;
xlabel("time") ; ylabel("rCEOF PCs") ;
datetick("mmm") ;
h = legend({"\\it\\eta^r" "\\it\\eta^i"}, "box", "off", "location", "northwest") ;
hpos = get(h, "position") ; hpos(2) += 0.05 ; set(h, "position", hpos) ;
ax(6) = subaxis(3,3,9, "SH", sh, "SV", sv) ;
plot(w(IY&IM,1),w(IY&IM,2), "k") ; box off ;
set(gca, "xaxislocation", "origin", "yaxislocation", "origin")
xlabel("\\it\\eta^r") ; ylabel("\\it\\eta^i") ; xticklabels([]) ; yticklabels([]) ;
axis square ;

print(sprintf("nc/olr.pc.svg")) ;

## lagged correlation
figure(1) ;
clf ; hold on ; maxlag = 20 ;
w = [EOF.pc(IV,1) EOF.pc(IV,2)] ; w(~IM,:) = NaN ;
[EOF.R lag] = xcor(w(:,1), w(:,2), maxlag) ;
w = [real(CEOF.pc(IV,jC)) imag(CEOF.pc(IV,jC))] ; w(~IM,:) = NaN ;
[CEOF.R lag] = xcor(w(:,1), w(:,2), maxlag) ;
w = [real(CEOF.cc(IV,jC)) imag(CEOF.cc(IV,jC))] ; w(~IM,:) = NaN ;
[CEOF.Rc lag] = xcor(w(:,1), w(:,2), maxlag) ;
h = plot(lag, EOF.R, "color", EOF.col, lag, CEOF.R, "color", CEOF.col, lag, CEOF.Rc, "color", rCEOF.col) ;
set(h, "linewidth", 3) ;
set(gca, "xaxislocation", "origin", "yaxislocation", "origin", "ygrid", "on") ;
xlabel("lag  [d]") ; ylabel("correlation") ;
h = legend({"EOF_{1/2}" "CEOF^{r/i}" "rCEOF^{r/i}"}, "box", "off", "location", "northeast") ;

print(sprintf("nc/olr.corr.svg")) ;

## spectrum
warning("off", "Octave:noninteger-range-as-index")
Nfft = 300 ; overlap = 0.5 ; Fs = 1 ;
J = 1:4 ;
figure(1, "position", [0.682   0.446   0.50   0.5])
clf ; clear ax ;

ax(1) = subplot(2,2,1) ; hold on
set(gca, "LinestyleOrder", {"-" "--"}) ;
[Cx(:,1),freq] = pwelch(EOF.pc(IV,1), Nfft, overlap, Fs) ;
[Cx(:,2),freq] = pwelch(EOF.pc(IV,2), Nfft, overlap, Fs) ;
h = plot(1./freq, Cx, "color", EOF.col) ;
set(h(2), "Linestyle", "--") ;
xlabel("period") ; set(gca, "xgrid", "on") ;
ylabel("power") ;
legend({"PC_1" "PC_2"}, "box", "off") ;

ax(2) = subplot(2,2,2) ; hold on
jC = 1 ;

[Cx(:,1),freq] = pwelch(real(CEOF.pc(IV,jC)), Nfft, overlap, Fs) ;
[Cx(:,2),freq] = pwelch(imag(CEOF.pc(IV,jC)), Nfft, overlap, Fs) ;
hP = plot(1./freq, Cx, "color", CEOF.col) ;
[Cx(:,1),freq] = pwelch(real(CEOF.cc(IV,jC)), Nfft, overlap, Fs) ;
q = std(imag(CEOF.pc(IM,jC))) / std(imag(CEOF.cc(IM,jC))) ;
[Cx(:,2),freq] = pwelch(imag(CEOF.cc(IV,jC)), Nfft, overlap, Fs) ;
hC = plot(1./freq, Cx, "color", rCEOF.col) ;
set([hP(2) hC(2)], "Linestyle", "--") ;
xlabel("period") ; set(gca, "xgrid", "on") ;
ylabel("power") ;
legend({"\\it\\xi^r" "\\it\\xi^i" "\\it\\eta^r" "\\it\\eta^i"}, "box", "off") ;

yl = max(cell2mat(get(ax, "ylim"))) ; set(ax, "ylim", yl) ;

subplot(2,2,3) ; hold on
[Cxy,freq] = mscohere(EOF.pc(IV,1), EOF.pc(IV,2), Nfft, overlap, Fs) ;
plot(1./freq, Cxy, "color", EOF.col) ;
[Cxy(:,1),freq] = mscohere(real(CEOF.pc(IV,jC)), imag(CEOF.pc(IV,jC)), Nfft, overlap, Fs) ;
plot(1./freq, Cxy, "color", CEOF.col) ;
[Cxy(:,1),freq] = mscohere(real(CEOF.cc(IV,jC)), imag(CEOF.cc(IV,jC)), Nfft, overlap, Fs) ;
plot(1./freq, Cxy, "color", rCEOF.col) ;
xlabel("period") ; set(gca, "xgrid", "on") ;
ylabel("squared coherency") ;
legend({"PC_{1/2}" "\\it\\xi^{r/i}" "\\it\\eta^{r/i}"}, "box", "off") ;

subplot(2,2,4) ; hold on
[Pxy,freq] = cpsd(EOF.pc(IV,1), EOF.pc(IV,2), Nfft, overlap, Fs) ;
plot(1./freq, -angle(Pxy), "color", EOF.col) ;
[Pxy(:,1),freq] = cpsd(real(CEOF.pc(IV,jC+1)), imag(CEOF.pc(IV,jC+1)), Nfft, overlap, Fs) ;
plot(1./freq, -angle(Pxy), "color", CEOF.col) ;
[Pxy(:,1),freq] = cpsd(real(CEOF.cc(IV,jC)), imag(CEOF.cc(IV,jC)), Nfft, overlap, Fs) ;
plot(1./freq, -angle(Pxy), "color", rCEOF.col) ;
xlabel("period") ; set(gca, "xgrid", "on") ;
ylabel("phase") ;
set(gca, "ylim", [0 pi], "ygrid", "on", "ytick", [0 pi/4 pi/2 3/4*pi], "yticklabel", {"0"  "{1/4\\pi}" "{\\pi/2}" "{3/4\\pi}"}) ;
legend({"PC_{1/2}" "\\it\\xi^{r/i}" "\\it\\eta^{r/i}"}, "box", "off") ;

set(findobj(gcf, "type", "line"), "linewidth", 2) ;
print(sprintf("nc/spec.svg")) ;

## causal non-causal coherence
set(gcf, "position", [0.700   0.700   0.300   0.500])
clf ;
subplot(2,1,1) ;
##[Cxy,freq] = mscohere(imag(Z(IV,1)), imag(Zh(IV,2)), Nfft, overlap, Fs) ;
[Cxy,freq] = mscohere(imag(CEOF.pc(IV,jC)), imag(CEOF.cc(IV,jC)), Nfft, overlap, Fs) ;
plot(1./freq, Cxy, "k") ;
xlabel("period") ; set(gca, "xgrid", "on") ;
ylabel("squared coherency") ;
legend({"\\it\\xi^{i}/\\it\\eta^{i}"}, "box", "off") ;
subplot(2,1,2) ;
##[Cxy,freq] = cpsd(imag(Z(IV,jC)), imag(Zh(IV,jC)), Nfft, overlap, Fs) ;
[Cxy,freq] = cpsd(imag(CEOF.pc(IV,jC)), imag(CEOF.cc(IV,jC)), Nfft, overlap, Fs) ;
plot(1./freq, -angle(Cxy), "k") ;
xlabel("period") ; set(gca, "xgrid", "on") ;
ylabel("phase") ;
set(gca, "ylim", [-pi/2 pi/2], "ytick", [-pi/2 0 pi/2], "yticklabel", {"-{\\pi/2}" "0" "{\\pi/2}"}) ;
legend({"\\it\\xi^{i}/\\it\\eta^{i}"}, "box", "off") ;
print(sprintf("nc/causal_spec.svg")) ;
r = corr(CEOF.pc(IV,jC), CEOF.cc(IV,jC)) ;
printf("r = %5.2f ; phs = %5.2f\n", abs(r), arg(r)) ;
r = corr2([real(CEOF.pc(IV,jC)) imag(CEOF.pc(IV,jC))], [real(CEOF.cc(IV,jC)) imag(CEOF.cc(IV,jC))]) ;
printf("r = %5.2f\n", r) ;


## explained variance
cl = [0 20] ;
jE = 1:2 ;					    # number of EOF
SFX = {"p" "c"} ; ttl = {"full HT" "causal HT"} ;
pos1 = pos ;
##pos1(4) *= 2.3 ; pos1(1) -= 0.25 ;
pos1(3) *= 2 ; pos1(1) -= 0.25 ; pos1(4) -= 0.0 ;
figure(2, "position", pos1) ; colormap(flip(gray)) ;
fmt = repmat("\t%.1f%%", 1, 1) ;

clf ; clear ax ;
jC = 1 ;
ax(1) = subplot(1,3,1) ; hold on ;
pa = get(gca, "position") ;
pos1 = pa ; pos1(4) = 0.5 ; pos1(2) += 0.2 ;
set(gca, "position", pos1, "clim", cl) ;
printf(["EOF #1+#2, %s:" fmt "\n"], vn, 100*rv(EOF.pc(IV,jE) * EOF.pat(:,jE)', X(IV,:))) ;
sev = reshape(EOF.pev(:,jC), [olr.N(2:3) 2]) ;
contourf(olr.lon, olr.lat, 100*sev(:,:,jvar)') ; colorbar("location", "northoutside") ;
colormap(gca, flip(gray)) ;
plot(ncst(:,1), ncst(:,2), "k")
xlabel("longitude") ; ylabel("latitude") ;
ht = title("EOF", "position", [100 65 0.5]) ;

jC = 1 ;
ax(2) = subplot(1,3,2) ; hold on ;
pa = get(gca, "position") ;
pos1 = pa ; pos1(4) = 0.5 ; pos1(2) += 0.2 ;
set(gca, "position", pos1) ;
printf(["CEOF #%d, %s:" fmt "\n"], jC, vn, 100*CEOF.ev(jC)) ;
sev = reshape(CEOF.pev(:,jC), [olr.N(2:3) 2]) ;
contourf(olr.lon, olr.lat, 100*sev(:,:,jvar)') ; colorbar("location", "northoutside") ;
colormap(gca, flip(gray)) ;
plot(ncst(:,1), ncst(:,2), "k")
xlabel("longitude") ; ylabel("latitude") ;
title("CEOF", "position", [100 65 0.5]) ;

ax(3) = subplot(1,3,3) ; hold on ;
pa = get(gca, "position") ;
pos1 = pa ; pos1(4) = 0.5 ; pos1(2) += 0.2 ;
set(gca, "position", pos1) ;
printf(["causal CEOF #%d, %s:" fmt "\n"], jC, vn, 100*CEOF.cv(jC)) ;
sev = reshape(CEOF.cev(:,jC), [olr.N(2:3) 2]) ;
contourf(olr.lon, olr.lat, 100*sev(:,:,jvar)') ; colorbar("location", "northoutside") ;
colormap(gca, flip(gray)) ;
plot(ncst(:,1), ncst(:,2), "k")
xlabel("longitude") ; ylabel("latitude") ;
h=title(rCEOFn, "position", [100 65 0.5]) ;

set(ax, "clim", cl) ;
set(gcf, "paperpositionmode", "auto") ;
##set(gcf, "paperposition", [0 0 1 1], "paperorientation", "landscape")
print(sprintf("nc/EV.%s.svg", vn)) ;


## composites (Kim et al. 2009)
jC = 1 ;
pkg load matgeom
EOF.z = zscore(EOF.pc(:,1:2)) ;
CEOF.z = zscore([real(CEOF.pc(:,jC)) imag(CEOF.pc(:,jC))]) ;
CEOF.zc = zscore([real(CEOF.cc(:,jC)) imag(CEOF.cc(:,jC))]) ;

EOF.amp = sqrt(sumsq(EOF.z, 2)) ;
EOF.phs = atan2(EOF.z(:,2), EOF.z(:,1)) ; EOF.phs(EOF.phs < 0) += 2*pi ;
CEOF.amp = sqrt(sumsq(CEOF.z, 2)) ;
CEOF.phs = atan2(CEOF.z(:,2), CEOF.z(:,1)) ; CEOF.phs(CEOF.phs < 0) += 2*pi ;
CEOF.ampc = sqrt(sumsq(CEOF.zc, 2)) ;
CEOF.phsc = atan2(CEOF.zc(:,2), CEOF.zc(:,1)) ; CEOF.phsc(CEOF.phsc < 0) += 2*pi ;

nPHS = 8 ; amp0 = 1.5 ; nr = olr.N(1) ; nc = 2 ;
PHS = linspace(0, 2*pi, nPHS+1) ; PHS = PHS(1:nPHS) - pi / (nPHS) ;
JPHS = [5 5 0] ;

sh = 0.1 ; sv = 0.005 ; clear ax? ; LC = "rec" ;
if strcmp(LC, "rec")
   cl = 0.09 ;
else
   cl = max(abs(olr.x(:))) ;
endif
ps = zeros(3, nPHS) ;
figure(1, "position", [0.7   0.85   0.3   0.15]) ;
figure(3, "position", [0.7   0.535   0.3   0.15], "defaultaxesfontname", "Linux Biolinum") ;
figure(4, "position", [0.7   0.325   0.3   0.15], "defaultaxesfontname", "Linux Biolinum") ;
npl = 3 ; CO = 0 * [1 0 0 ; 1 0 0 ; 0 0 1 ; 0 0 1] ; col = 0.7*ones(nPHS, 3) ; col(1,:) = [0 0 0] ;
pos1 = pos ; pos1(1) = 0.45 ; pos1(3) *= 0.8 ; pos1(4) *= 2 ;
figure(2, "position", pos1) ;
clf(1) ; clf(2) ; clf(3) ; clf(4) ;
jpl = 1 ;
figure(1) ; ax1(jpl) = subplot(1,npl,jpl) ; hold on
set(ax1(jpl), "ColorOrder", CO) ;
Ia = EOF.amp > amp0 ;
ps = (EOF.phs(find(Ia)+1)-EOF.phs(find(Ia))) ; ps(ps < -pi) += 2*pi ; ps(ps > pi) -= 2*pi ;
phs = shift(PHS, JPHS(jpl)) ; phs = [phs phs(1)] ;
for j = 1:nPHS
   if phs(j) < 0 & phs(j+1) >= 0 Ip = phs(j) + 2*pi <= EOF.phs | EOF.phs < phs(j+1) ; endif
   if phs(j) >= 0 & phs(j+1) < 0 Ip = phs(j) <= EOF.phs & EOF.phs < phs(j+1) + 2*pi ; endif
   if phs(j) < 0 & phs(j+1) < 0 Ip = phs(j) + 2*pi <= EOF.phs & EOF.phs < phs(j+1) + 2*pi ; endif
   if phs(j) >= 0 & phs(j+1) >= 0 Ip = phs(j) <= EOF.phs & EOF.phs < phs(j+1) ; endif
   I = find(Ia & Ip) ;
   EOF.lc.N(j) = length(I) ;
   EOF.lc.pc(:,:,j) = cell2mat(arrayfun(@(d) mean(EOF.z(min(I+d,nr),:)), (0:nd)', "UniformOutput", false)) ;
   EOF.lc.std(j) = mean(cell2mat(arrayfun(@(d) std(EOF.amp(min(I+d,nr),:)), (0:nd)', "UniformOutput", false))) ;
   Ir = EOF.phs(I) > mean(EOF.phs(I)) + pi/4 ; EOF.lc.ps(j,1) = mean([EOF.phs(I)(Ir) - 2*pi ; EOF.phs(I)(~Ir)]) ;  
   EOF.lc.ps(j,2) = mean(cell2mat(arrayfun(@(d) mean(phsdiff(EOF.phs(min(I+d+1,nr)), EOF.phs(min(I+d,nr)))), (0:nd)', "UniformOutput", false))) ;
   scatter(ax1(jpl), EOF.lc.pc(:,1,j), EOF.lc.pc(:,2,j), 10, "filled")
   scatter(ax1(jpl), EOF.lc.pc(1,1,j), EOF.lc.pc(1,2,j), 40, col(j,:), "filled")
   set(ax1(jpl), "xticklabel", [], "yticklabel", []) ;
   figure(2) ;
   ax(2,nc*j-(nc-1)) = subaxis(nPHS, nc, nc*j-(nc-1), "SpacingHoriz", sh, "SpacingVert", sv) ; hold on ;
   if strcmp(LC, "rec")
      EOF.lc.pat(j,:,:,:) = reshape(EOF.lc.pc(1,:,j) * EOF.pat(:,1:2)', [olr.N(2:3) 2]) ;
   else
      EOF.lc.pat(j,:,:,1) = squeeze(mean(olr.x(I,:,:))) ;
      EOF.lc.pat(j,:,:,2) = squeeze(mean(uwnd.x(I,:,:))) ;
   endif
   contourf(olr.lon, olr.lat, w=squeeze(EOF.lc.pat(j,:,:,jvar))') ;
##   cl = max(abs(squeeze(EOF.lc.pat(j,:,:,jvar))(:)))
   set(gca, "clim", [-cl cl]) ;
   plot(ncst(:,1), ncst(:,2), "k")
   switch j
      case 1
	 title("EOF") ;
	 set(gca, "xticklabel", [], "yticklabel", []) ;
	 text(20, 30, sprintf("P%d", j)) ;
      case nPHS
	 pi ;
      otherwise
	 set(gca, "xticklabel", [], "yticklabel", []) ;
	 text(20, 30, sprintf("P%d", j)) ;
   endswitch
   colormap(gca, flip(redblue)) ;
endfor
figure(3) ; ax3(jpl) = subplot(1,npl,jpl) ;
ap(jpl,1) = polar(EOF.lc.ps([1:end 1],1), EOF.lc.ps([1:end 1],2), "-o") ;
set(ax3(jpl), "Nextplot", "Add") ;
ap(jpl,2) = polar(EOF.lc.ps(1,1), EOF.lc.ps(1,2), "-o") ;
ht = title("EOF") ;
figure(4) ; ax4(jpl) = subplot(1,npl,jpl) ;
ap4(jpl,1) = polar(EOF.lc.ps([1:end 1],1), EOF.lc.std([1:end 1]), "-o") ;
set(ax4(jpl), "Nextplot", "Add") ;
ap4(jpl,2) = polar(EOF.lc.ps(1,1), EOF.lc.std(1), "-o") ;
title("EOF") ;
figure(1) ;
h = drawShape ('circle', [0 0 1], "fill", "parent", 1) ;
xlabel("PC_1") ; ylabel("PC_2") ;
set(h, "facecolor", 0.7*[1 1 1], "facealpha", 0.7);
title("EOF") ;

jpl = 2 ;
ax1(jpl) = subplot(1,npl,jpl) ; hold on
set(gca, "ColorOrder", CO) ;
Ia = CEOF.amp > amp0 ;
ps = (CEOF.phs(find(Ia)+1)-CEOF.phs(find(Ia))) ; ps(ps < -pi) += 2*pi ; ps(ps > pi) -= 2*pi ;
phs = shift(PHS, JPHS(jpl)) ; phs = [phs phs(1)] ;
for j = 1:nPHS
   if phs(j) < 0 & phs(j+1) >= 0 Ip = phs(j) + 2*pi <= CEOF.phs | CEOF.phs < phs(j+1) ; endif
   if phs(j) >= 0 & phs(j+1) < 0 Ip = phs(j) <= CEOF.phs & CEOF.phs < phs(j+1) + 2*pi ; endif
   if phs(j) < 0 & phs(j+1) < 0 Ip = phs(j) + 2*pi <= CEOF.phs & CEOF.phs < phs(j+1) + 2*pi ; endif
   if phs(j) >= 0 & phs(j+1) >= 0 Ip = phs(j) <= CEOF.phs & CEOF.phs < phs(j+1) ; endif
   I = find(Ia & Ip) ;
   CEOF.lc.N(j) = length(I) ;
   CEOF.lc.pc(:,:,j) = cell2mat(arrayfun(@(d) mean(CEOF.z(min(I+d,nr),:)), (0:nd)', "UniformOutput", false)) ;
   CEOF.lc.std(j) = mean(cell2mat(arrayfun(@(d) std(CEOF.amp(min(I+d,nr),:)), (0:nd)', "UniformOutput", false))) ;
   Ir = CEOF.phs(I) > mean(CEOF.phs(I)) + pi/4 ; CEOF.lc.ps(j,1) = mean([CEOF.phs(I)(Ir) - 2*pi ; CEOF.phs(I)(~Ir)]) ;
   CEOF.lc.ps(j,2) = mean(cell2mat(arrayfun(@(d) mean(phsdiff(CEOF.phs(min(I+d+1,nr)), CEOF.phs(min(I+d,nr)))), (0:nd)', "UniformOutput", false))) ;
   scatter(ax1(jpl), CEOF.lc.pc(:,1,j), CEOF.lc.pc(:,2,j), 10, "filled")
   scatter(ax1(jpl), CEOF.lc.pc(1,1,j), CEOF.lc.pc(1,2,j), 40, col(j,:), "filled")
   set(ax1(jpl), "xticklabel", [], "yticklabel", []) ;
   figure(2) ;
   ax(2,nc*j-(nc-2)) = subaxis(nPHS, nc, nc*j-(nc-2), "SpacingHoriz", sh, "SpacingVert", sv) ; hold on ;
   if strcmp(LC, "rec")
      CEOF.lc.pat(j,:,:,:) = reshape(CEOF.lc.pc(1,:,j) * [real(CEOF.pat(:,jC)) imag(CEOF.pat(:,jC))]', [olr.N(2:3) 2]) ;
   else
      CEOF.lc.pat(j,:,:,1) = squeeze(mean(olr.x(I,:,:))) ;
      CEOF.lc.pat(j,:,:,2) = squeeze(mean(uwnd.x(I,:,:))) ;
   endif
   contourf(olr.lon, olr.lat, w=squeeze(CEOF.lc.pat(j,:,:,jvar))') ;
##   cl = max(abs(squeeze(CEOF.lc.pat(j,:,:,jvar))(:)))
   set(gca, "clim", [-cl cl]) ;
   plot(ncst(:,1), ncst(:,2), "k")
   switch j
      case 1
	 title("CEOF") ;
	 set(gca, "xticklabel", [], "yticklabel", []) ;
	 text(170, 30, sprintf("P%d", j)) ;
      case nPHS
	 set(gca, "yaxislocation", "right") ;
      otherwise
	 set(gca, "xticklabel", [], "yticklabel", []) ;
	 text(170, 30, sprintf("P%d", j)) ;
   endswitch
   colormap(gca, flip(redblue)) ;
endfor
figure(3) ; ax3(jpl) = subplot(1,npl,jpl) ;
ap(jpl,1) = polar(CEOF.lc.ps([1:end 1],1), CEOF.lc.ps([1:end 1],2), "-o") ;
set(ax3(jpl), "Nextplot", "Add") ;
ap(jpl,2) = polar(CEOF.lc.ps(1,1), CEOF.lc.ps(1,2), "-o") ;
title("CEOF") ;
figure(4) ; ax4(jpl) = subplot(1,npl,jpl) ;
ap4(jpl,1) = polar(CEOF.lc.ps([1:end 1],1), CEOF.lc.std([1:end 1]), "-o") ;
set(ax4(jpl), "Nextplot", "Add") ;
ap4(jpl,2) = polar(CEOF.lc.ps(1,1), CEOF.lc.std(1), "-o") ;
title("CEOF") ;
figure(1) ;
h = drawShape ('circle', [0 0 1], "fill", "parent", 1) ;
xlabel("\\it\\xi^r") ; ylabel("\\it\\xi^i") ;
set(h, "facecolor", 0.7*[1 1 1], "facealpha", 0.7);
title("CEOF") ;

jpl = 3 ;
ax1(jpl) = subplot(1,npl,jpl) ; hold on
set(gca, "ColorOrder", CO) ;
Ia = CEOF.ampc > amp0 ;
ps = (CEOF.phsc(find(Ia)+1)-CEOF.phsc(find(Ia))) ; ps(ps < -pi) += 2*pi ; ps(ps > pi) -= 2*pi ;
phs = shift(PHS, JPHS(jpl)) ; phs = [phs phs(1)] ;
for j = 1:nPHS
   if phs(j) < 0 & phs(j+1) >= 0 Ip = phs(j) + 2*pi <= CEOF.phsc | CEOF.phsc < phs(j+1) ; endif
   if phs(j) >= 0 & phs(j+1) < 0 Ip = phs(j) <= CEOF.phsc & CEOF.phsc < phs(j+1) + 2*pi ; endif
   if phs(j) < 0 & phs(j+1) < 0 Ip = phs(j) + 2*pi <= CEOF.phsc & CEOF.phsc < phs(j+1) + 2*pi ; endif
   if phs(j) >= 0 & phs(j+1) >= 0 Ip = phs(j) <= CEOF.phsc & CEOF.phsc < phs(j+1) ; endif
   I = find(Ia & Ip) ;
   CEOF.lc.Nc(j) = length(I) ;
   CEOF.lc.cc(:,:,j) = cell2mat(arrayfun(@(d) mean(CEOF.zc(min(I+d,nr),:)), (0:nd)', "UniformOutput", false)) ;
   CEOF.lc.stdc(j) = mean(cell2mat(arrayfun(@(d) std(CEOF.ampc(min(I+d,nr),:)), (0:nd)', "UniformOutput", false))) ;
   CEOF.lc.scc(:,j) = cell2mat(arrayfun(@(d) complex(std(real(CEOF.zc(min(I+d,nr)))), std(imag(CEOF.zc(min(I+d,nr))))), (0:nd)', "UniformOutput", false)) ;
   Ir = CEOF.phsc(I) > mean(CEOF.phsc(I)) + pi/4 ; CEOF.lc.psc(j,1) = mean([CEOF.phsc(I)(Ir) - 2*pi ; CEOF.phsc(I)(~Ir)]) ;
   CEOF.lc.psc(j,2) = mean(cell2mat(arrayfun(@(d) mean(phsdiff(CEOF.phsc(min(I+d+1,nr)), CEOF.phsc(min(I+d,nr)))), (0:nd)', "UniformOutput", false))) ;
   scatter(ax1(jpl), CEOF.lc.cc(:,1,j), CEOF.lc.cc(:,2,j), 10, "filled")
   scatter(ax1(jpl), CEOF.lc.cc(1,1,j), CEOF.lc.cc(1,2,j), 40, col(j,:), "filled")
   set(ax1(jpl), "xticklabel", [], "yticklabel", []) ;
   if nc < 3 continue ; endif
   figure(2) ;
   ax(2,nc*j) = subaxis(nPHS, nc, nc*j, "SpacingHoriz", sh, "SpacingVert", sv) ; hold on ;
   if strcmp(LC, "rec")
      CEOF.lc.patc(j,:,:,:) = reshape(real(CEOF.lc.cc(1,j) * CEOF.pat(:,jC)'), [olr.N(2:3) 2]) ;
   else
      CEOF.lc.patc(j,:,:,1) = squeeze(mean(olr.x(I,:,:))) ;
      CEOF.lc.patc(j,:,:,2) = squeeze(mean(uwnd.x(I,:,:))) ;
   endif
   contourf(olr.lon, olr.lat, w=squeeze(CEOF.lc.patc(j,:,:,jvar))') ;
##   cl = max(abs(squeeze(CEOF.lc.patc(j,:,:,jvar))(:))) ;
   set(gca, "clim", [-cl cl]) ;
   plot(ncst(:,1), ncst(:,2), "k")
   switch j
      case 1
	 title(rCEOFn) ;
	 set(gca, "xticklabel", [], "yticklabel", []) ;
      case nPHS
	 pi ;
      otherwise
	 set(gca, "xticklabel", [], "yticklabel", []) ;
   endswitch
   colormap(gca, flip(redblue)) ;
endfor
figure(3) ; ax3(jpl) = subplot(1,npl,jpl) ;
ap(jpl,1) = polar(CEOF.lc.psc([1:end 1],1), CEOF.lc.psc([1:end 1],2), "-o") ;
set(ax3(jpl), "Nextplot", "Add") ;
ap(jpl,2) = polar(CEOF.lc.psc(1,1), CEOF.lc.psc(1,2), "-o") ;
title(rCEOFn) ;
figure(4) ; ax4(jpl) = subplot(1,npl,jpl) ;
ap4(jpl,1) = polar(CEOF.lc.ps([1:end 1],1), CEOF.lc.stdc([1:end 1]), "-o") ;
set(ax4(jpl), "Nextplot", "Add") ;
ap4(jpl,2) = polar(CEOF.lc.ps(1,1), CEOF.lc.stdc(1), "-o") ;
title("rCEOF") ;
figure(1) ;
h = drawShape ('circle', [0 0 1], "fill", "parent", 1) ;
xlabel("\\it\\eta^r") ; ylabel("\\it\\eta^i") ;
set(h, "facecolor", 0.7*[1 1 1], "facealpha", 0.7);
title(rCEOFn) ;

set(ax1, "xlim", [-2.2 2.2], "ylim", [-2.2 2.2], "plotboxaspectratio", [1 1 1], "xaxislocation", "origin", "yaxislocation", "origin")
set(ax(2,:), "xtick", [40 80 120 160], "ytick", [0 20 40]) ;
phs = PHS ; phs(phs<0) += 2*pi ;
set(ap(:,1), "color", 0.0*[1 1 1], "markerfacecolor", 0.7*[1 1 1], "markeredgecolor", 0.7*[1 1 1]) ;
set(ap(:,2), "markerfacecolor", 0.0*[1 1 1], "markeredgecolor", 0.0*[1 1 1]) ;
xl = 0.27 ;
set(ax3, "xlim", [-xl xl], "ylim", [-xl xl], "rtick", [0 0.1 0.2], "ttick", [0 90 180 270]) ;
cellfun(@(c) set(findall(3, "string", c), "string", ""), {"0" "90" "180" "270"}) ;
set(ap4(:,1), "color", 0.0*[1 1 1], "markerfacecolor", 0.7*[1 1 1], "markeredgecolor", 0.7*[1 1 1]) ;
set(ap4(:,2), "markerfacecolor", 0.0*[1 1 1], "markeredgecolor", 0.0*[1 1 1]) ;
set(ax4, "rtick", [0:0.2:0.8], "ttick", [0 90 180 270]) ;
cellfun(@(c) set(findall(4, "string", c), "string", ""), {"0" "90" "180" "270"}) ;

figure(2) ;
hc = colorbar("south") ;
set(hc, "xaxislocation", "bottom")
set(hc, "position", [0.1 0.04 0.8 0.01]) ;

set(1:3, "paperpositionmode", "auto") ;
print(1, sprintf("nc/lc.%s.pc.svg", vn)) ;
print(2, sprintf("nc/lc.%s.pat.svg", vn)) ;
print(3, sprintf("nc/lc.%s.ps.svg", vn)) ;


## other indices
jC = 1 ;
## (R)OMI Kiladis et al. 2014; (r)PII, cf. Wang 2020
EOF.id = CEOF.id = olr.id ;
EOF.IM = CEOF.IM = rCEOF.IM = ismember(EOF.id(:,2), MON) ;
[K12 rK12] = read_K12("data/BSISO_25-90bpfil_pc.txt", "data/BSISO_25-90bpfil.rt_pc.txt", MON) ;
[OMI rOMI] = read_OMI("data/omi.1x.txt", "data/romi.1x.txt", MON) ;
[PII rPII] = read_PII("data/PII_EOFs.nc", MON) ;
RMM = read_RMM("data/rmm.74toRealtime.txt", MON) ;
OMI.col = rOMI.col = g0*[1 0 1] ;
K12.col = rK12.col = [1 1 0] ;
PII.col = rPII.col = g0*[0 1 1] ;
RMM.col = g0*[1 1 0] ;

[EOF.I OMI.I K12.I PII.I RMM.I] = common(EOF.id, OMI.id, K12.id, PII.id, RMM.id) ;
EOF.x = nrm(EOF.pc, EOF.I & EOF.IM) ;
CEOF.x = nrm([real(CEOF.pc(:,jC)) imag(CEOF.pc(:,jC))], EOF.I & EOF.IM) ;
OMI.x = nrm(OMI.x, OMI.I & OMI.IM) ;
K12.x = nrm(K12.x, K12.I & K12.IM) ;
PII.x = nrm(PII.x, PII.I & PII.IM) ;
RMM.x = nrm(RMM.x, RMM.I & RMM.IM) ;
[EOF.I rOMI.I rK12.I rPII.I] = common(EOF.id, rOMI.id, rK12.id, rPII.id) ;
rCEOF.x = nrm([real(CEOF.cc(:,jC)) imag(CEOF.cc(:,jC))], EOF.I & EOF.IM) ;
rOMI.x = nrm(rOMI.x, rOMI.I & rOMI.IM) ;
rK12.x = nrm(rK12.x, rK12.I & rK12.IM) ;
rPII.x = nrm(rPII.x, rPII.I & rPII.IM) ;

## K12 strangeness
[I rI] = common(K12.id, rK12.id) ;
corr2(K12.x(I,:), rK12.x(rI,:))
[I I2] = common(K12.id, CEOF.id) ;
K12.z = complex(K12.x(:,1), K12.x(:,2)) ;
disp(abs(corr2(K12.z(I,:), CEOF.pc(I2,1)))) ;

## test phase
y = 2018 ; m = 7 ;
test_phs(EOF, y, m, p = [2 1], s = [1 -1]) ;
EOF.p = p ; EOF.s = s ;
y = 2018 ; m = 6 ;
y = 2009 ; m = 10 ;
test_phs(RMM, y, m, p = [1 2], s = [1 1]) ;
RMM.p = p ; RMM.s = s ;
y = 2010 ; m = 1 ;
test_phs(OMI, y, m, p = [2 1], s = [1 -1]) ;
OMI.p = rOMI.p = p ; OMI.s = rOMI.s = s ;
test_phs(K12, y, m, p = [1 2], s = [-1 -1]) ;
K12.p = rK12.p = p ; K12.s = rK12.s = s ;
y = 2011 ; m = 10 ;
h = test_phs(CEOF, y, m, p = [2 1], s = [1 -1]) ;
CEOF.p = rCEOF.p = p ; CEOF.s = rCEOF.s = s ;
test_phs(PII, y, m, p = [1 2], s = [1 1]) ;
PII.p = rPII.p = p ; PII.s = rPII.s = s ;

## correlation between indices
##EOF.p = [2 1] ; EOF.s = [1 -1] ;
output_precision(3)
SS = {"EOF" "CEOF" "OMI" "PII" "RMM"} ;
rSS = {"EOF" "rCEOF" "rOMI" "rPII" "RMM"} ;
Y = 2002:2017 ;
EOF.I = EOF.I = CEOF.I = ismember(EOF.id(:,1), Y) ;
OMI.I = ismember(OMI.id(:,1), Y) ;
K12.I = ismember(K12.id(:,1), Y) ;
PII.I = ismember(PII.id(:,1), Y) ;
RMM.I = ismember(RMM.id(:,1), Y) ;
##[EOF.I OMI.I PII.I] = common(EOF.id, OMI.id, PII.id) ;
LR = LC = RC = CC = nan(length(SS), length(SS)) ;
for j = 2:length(SS)
   u = eval(sprintf("%s", SS{j})) ;
   u.z = complex(u.x(:,1), u.x(:,2)) ;
   for k = 1:j-1
      v = eval(sprintf("%s", SS{k})) ;
      RX = arrayfun(@(l) corr2(shiftg(u.x(u.I & u.IM,u.p) .* u.s, l), v.x(v.I & v.IM,v.p) .* v.s), L=-20:20) ;
      v.z = complex(v.x(:,1), v.x(:,2)) ;
      CX = arrayfun(@(l) corr2(shiftg(u.z(u.I & u.IM,1), l), v.z(v.I & v.IM,1)), L=-20:20) ;
      [~, m] = max(abs(CX)) ;
      LC(k,j) = L(m) ; CC(k,j) = CX(m) ;
      [~, m] = max(abs(RX)) ;
      LR(k,j) = L(m) ; RC(k,j) = RX(m) ;
   endfor
endfor
##disp(LR(1:end-1,2:end)) ; disp(RC(1:end-1,2:end)) ;
disp(LC(1:end-1,2:end)) ; disp(abs(CC)(1:end-1,2:end)) ;
##status = xlswrite ('/tmp/test.ods', RC) ;

rCEOF.I = ismember(EOF.id(:,1), Y) ;
rCEOF.IM = ismember(EOF.id(:,2), MON) ;
rOMI.I = ismember(rOMI.id(:,1), Y) ;
rK12.I = ismember(rK12.id(:,1), Y) ;
rPII.I = ismember(rPII.id(:,1), Y) ;
RMM.I = ismember(RMM.id(:,1), Y) ;
LR = LC = RC = CC = nan(length(SS), length(SS)) ;
for j = 2:length(rSS)
   u = eval(sprintf("%s", rSS{j})) ;
   u.z = complex(u.x(:,1), u.x(:,2)) ;
   for k = 1:j-1
      v = eval(sprintf("%s", rSS{k})) ;
      RX = arrayfun(@(l) corr2(shiftg(u.x(u.I & u.IM,u.p) .* u.s, l), v.x(v.I & v.IM,v.p) .* v.s), L=-20:20) ;
      v.z = complex(v.x(:,1), v.x(:,2)) ;
      CX = arrayfun(@(l) corr2(shiftg(u.z(u.I & u.IM,1), l), v.z(v.I & v.IM,1)), L=-20:20) ;
      [~, m] = max(abs(CX)) ;
      LC(k,j) = L(m) ; CC(k,j) = CX(m) ;
      [~, m] = max(abs(RX)) ;
      LR(k,j) = L(m) ; RC(k,j) = RX(m) ;
   endfor
endfor
##disp(LR(1:end-1,2:end)) ; disp(RC(1:end-1,2:end)) ;
disp(LC(1:end-1,2:end)) ; disp(abs(CC)(1:end-1,2:end)) ;

SSN = {"EOF" "CEOF" "OMI" "PII" "RMM"} ;
rCEOF.col = CEOF.col ; clear ax CX b ;
figure(1) ; clf ;
set(gcf, "position", [0.750   0.700   0.250   0.500])
ax(1) = subplot(2,1,1) ; cla ; hold on ;
ax(2) = subplot(2,1,2) ; cla ; hold on ;
for j = 1:length(SS)
   ss = SS{j} ; rss = rSS{j} ;
   s = eval(sprintf("%s", ss)) ;
   rs = eval(sprintf("%s", rss)) ;
   s.z = complex(s.x(:,1), s.x(:,2)) ;
   rs.z = complex(rs.x(:,1), rs.x(:,2)) ;
##   CX = arrayfun(@(l) corr2(shiftg(s.z(s.I & s.IM,1), l), s.z(s.I & s.IM,1)), L=0:50) ;
   CX(:,j) = arrayfun(@(l) corr2(shiftg(s.z(s.I & s.IM,1), l), rs.z(rs.I & rs.IM,1)), L=0:50) ;
   eval(sprintf("plot(ax(1), L(1:50)', abs(CX(1:50,j)), \"color\", [%f %f %f]) ;", s.col)) ;
   eval(sprintf("plot(ax(2), L', phsadj(arg(CX(:,j))), \"color\", [%f %f %f]) ;", s.col)) ;
   eval(sprintf("b(j) = regress(phsadj(arg(CX(:,j))), L') ;")) ;
endfor
set(ax, "xaxislocation", "origin", "yaxislocation", "origin", "xlabel", "lag  [d]") ;
axes(ax(1)) ;
ylabel("| {\\it\\rho} |") ;
ylim([0 1]) ; grid on
h = legend(SSN, "box", "off", "location", "northeast") ;
axes(ax(2)) ;
ylabel("cum. phase") ;
h = legend(SSN, "box", "off", "location", "southeast") ;
printf("period: %7.2f = %2.0fd\n", mean(b), (2*pi)/mean(b))
set(findobj(gcf, "type", "line"), "linewidth", 2) ;
print(sprintf("nc/cplx.corr.svg")) ;

set(gcf, "position", [0.75   0.70   0.30   0.30])
clf ; hold on
plot(L(1:10), 100*0.14*(2*abs(CX(1:10,1))' - 1), "color", EOF.col) ;
plot(L(1:10), 100*0.11*(2*abs(CX(1:10,2))' - 1), "color", CEOF.col) ;
grid off
set(gca, "ylim", [-5 14], "xaxislocation", "origin", "xlabel", "lag  [d]") ;
xlabel("lag  [d]") ; ylabel("P • V  [%]") ;
h = legend(SSN(1:2), "box", "off", "location", "northeast") ;
print(sprintf("nc/PV.svg")) ;


## full indices
Y = [2018 1 ; 2018 12] ;
olr.I = olr.Ir = sdate(olr.id, Y) ;
OMI.I = sdate(OMI.id, Y) ;
rOMI.I = sdate(rOMI.id, Y) ;
PII.I = rPII.I = sdate(PII.id, Y) ;

set(0, "defaultaxesfontname", "Linux Biolinum", "defaulttextfontname", "Linux Biolinum") ;
clf ;
ax(1) = subplot(2,1,1) ;
hold on
plot(dateax(olr.id(olr.I,:)), EOF.x(olr.I,1), "color", EOF.col) ;
plot(dateax(olr.id(olr.I,:)), CEOF.x(olr.I,1), "color", CEOF.col) ;
plot(dateax(OMI.id(OMI.I,:)), OMI.x(OMI.I,1), "color", OMI.col) ;
plot(dateax(PII.id(PII.I,:)), -PII.x(PII.I,2), "color", PII.col) ;
set(findobj(gca, "type", "line"), "linewidth", 1.5) ;
datetick("mmm") ;
set(gca, "ygrid", "off", "box", "off", "fontsize", 14) ;
xlabel("time") ; ylabel("norm. indices") ;
title("Indices during 2018") ;
hl(1) = legend({"EOF_1" "CEOF^r" "OMI_1" "–PII_2"}, "box", "off", "location", "southeastoutside") ;
ax(2) = subplot(2,1,2) ;
hold on
plot(dateax(olr.id(olr.I,:)), EOF.x(olr.I,1), "color", EOF.col) ;
plot(dateax(olr.id(olr.I,:)), rCEOF.x(olr.I,1), "color", CEOF.col) ;
plot(dateax(rOMI.id(rOMI.I,:)), rOMI.x(rOMI.I,1), "color", OMI.col) ;
plot(dateax(PII.id(PII.I,:)), -rPII.x(rPII.I,2), "color", PII.col) ;
set(findobj(gca, "type", "line"), "linewidth", 1.5) ;
datetick("mmm") ;
set(gca, "ygrid", "off", "box", "off", "fontsize", 14) ;
xlabel("time") ; ylabel("norm. indices") ;
title("real time version") ;
hl(2) = legend({"EOF_1" [rCEOFn "^r"] "rOMI_1" "–rPII_2"}, "box", "off", "location", "northeastoutside") ;
set(hl, "fontsize", 12) ;
print(sprintf("nc/case_ts.svg")) ;


## the year 2018
pkg load matgeom
Y = [2018 7 1 ; 2018 7 31] ; al = 2.7 ; 
Y = [2009 11 1 ; 2009 11 31] ; al = 2.9 ; 
EOF.I = CEOF.I = rCEOF.I = sdate(olr.id, Y) ;
OMI.I = sdate(OMI.id, Y) ;
rOMI.I = sdate(rOMI.id, Y) ;
PII.I = sdate(PII.id, Y) ;
rPII.I = sdate(rPII.id, Y) ;
RMM.I = sdate(RMM.id, Y) ;

str = "\n\n\n" ;
AT = {"South China Sea" "India" "Indian Ocean" "Western North Pacific"} ;
tc = [-1.7 al+0.4 ; al+0.4 0.5 ; -1 -al-0.4 ; -al-0.4 -2.0] ;
sv = 0.18 ; ml = mr = 0.1 ; lw = [3 0.5] ;
clf ; clear h hc ht
set(gcf, "position", [0.700   0.700   0.350   0.500])

ax(1,1) = subaxis(2, 2, 1, "SpacingVert", sv, "ml", ml, "mr", mr) ; hold on
s = EOF ;
s.x = s.x(:,s.p) .* s.s ;
phsplot(s.x(:,1), s.x(:,2), s.I, s.col, "linewidth", lw(1)) ;
scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, s.col, "filled") ;
h(1,1) = drawShape ('circle', [0 0 1], "fill", "parent", 1) ;
hc(1,1) = drawShape ('circle', [0 0 2], "parent", 1) ;
title(["EOF" str]) ;
ht(1,1,1) = text(tc(1,1), tc(1,2), AT{1}, "rotation", 0) ;
ht(1,1,2) = text(tc(2,1), tc(2,2), AT{2}, "rotation", -90) ;
ht(1,1,3) = text(tc(3,1), tc(3,2), AT{3}, "rotation", 0) ;
ht(1,1,4) = text(tc(4,1), tc(4,2), AT{4}, "rotation", 90) ;

ax(1,2) = subaxis(2, 2, 2, "SpacingVert", sv, "ml", ml, "mr", mr) ; hold on
s = CEOF ; rs = rCEOF ;
s.x = s.x(:,s.p) .* s.s ; rs.x = rs.x(:,s.p) .* s.s ;
phsplot(s.x(:,1), s.x(:,2), s.I, s.col, "linewidth", lw(1)) ;
scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, s.col, "filled") ;
phsplot(rs.x(:,1), rs.x(:,2), rs.I, s.col, "linewidth", lw(2)) ;
scatter(rs.x(rs.I,1)(1), rs.x(rs.I,2)(1), 80, s.col, "filled") ;
h(1,2) = drawShape ('circle', [0 0 1], "fill", "parent", 1) ;
hc(1,2) = drawShape ('circle', [0 0 2], "parent", 1) ;
title(["CEOF" str]) ;
ht(1,2,1) = text(tc(1,1), tc(1,2), AT{1}, "rotation", 0) ;
ht(1,2,2) = text(tc(2,1), tc(2,2), AT{2}, "rotation", -90) ;
ht(1,2,3) = text(tc(3,1), tc(3,2), AT{3}, "rotation", 0) ;
ht(1,2,4) = text(tc(4,1), tc(4,2), AT{4}, "rotation", 90) ;

ax(2,1) = subaxis(2, 2, 3, "SpacingVert", sv, "ml", ml, "mr", mr) ; hold on
s = OMI ; rs = rOMI ;
s.x = s.x(:,s.p) .* s.s ; rs.x = rs.x(:,s.p) .* s.s ;
phsplot(s.x(:,1), s.x(:,2), s.I, s.col, "linewidth", lw(1)) ;
scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, s.col, "filled") ;
phsplot(rs.x(:,1), rs.x(:,2), rs.I, s.col, "linewidth", lw(2)) ;
scatter(rs.x(rs.I,1)(1), rs.x(rs.I,2)(1), 80, s.col, "filled") ;
s = PII ; rs = rPII ;
s.x = s.x(:,s.p) .* s.s ; rs.x = rs.x(:,s.p) .* s.s ;
phsplot(s.x(:,1), s.x(:,2), s.I, s.col, "linewidth", lw(1)) ;
scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, s.col, "filled") ;
phsplot(rs.x(:,1), rs.x(:,2), rs.I, s.col, "linewidth", lw(2)) ;
scatter(rs.x(rs.I,1)(1), rs.x(rs.I,2)(1), 80, s.col, "filled") ;
h(2,1) = drawShape ('circle', [0 0 1], "fill", "parent", 1) ;
hc(2,1) = drawShape ('circle', [0 0 2], "parent", 1) ;
title(["OMI/PII" str]) ;
ht(2,1,1) = text(tc(1,1), tc(1,2), AT{1}, "rotation", 0) ;
ht(2,1,2) = text(tc(2,1), tc(2,2), AT{2}, "rotation", -90) ;
ht(2,1,3) = text(tc(3,1), tc(3,2), AT{3}, "rotation", 0) ;
ht(2,1,4) = text(tc(4,1), tc(4,2), AT{4}, "rotation", 90) ;

ax(2,2) = subaxis(2, 2, 4, "SpacingVert", sv, "ml", ml, "mr", mr) ; hold on
s = RMM ;
s.x = s.x(:,s.p) .* s.s ;
phsplot(s.x(:,1), s.x(:,2), s.I, s.col, "linewidth", lw(1)) ;
scatter(s.x(s.I,1)(1), s.x(s.I,2)(1), 80, s.col, "filled") ;
h(2,2) = drawShape ('circle', [0 0 1], "fill", "parent", 1) ;
hc(2,2) = drawShape ('circle', [0 0 2], "parent", 1) ;
title(["RMM" str]) ;
ht(2,2,1) = text(tc(1,1), tc(1,2), AT{1}, "rotation", 0) ;
ht(2,2,2) = text(tc(2,1), tc(2,2), AT{2}, "rotation", -90) ;
ht(2,2,3) = text(tc(3,1), tc(3,2), AT{3}, "rotation", 0) ;
ht(2,2,4) = text(tc(4,1), tc(4,2), AT{4}, "rotation", 90) ;

set(h, "facecolor", 0.7*[1 1 1], "facealpha", 0.7);
set(hc, "color", [0 0 0], "linestyle", "--") ;
set(ax, "xlim", [-al al], "ylim", [-al al], "plotboxaspectratio", [1 1 1], "xaxislocation", "origin", "yaxislocation", "origin", "xtick", [], "ytick", [], "box", "off") ;

print(sprintf("nc/%4d_pp.svg", Y(1,1))) ;


## propagation
[bw.b bw.a] = butter(3, [1/70 1/20]) ;
set(gcf, "position", [0.700   0.700   0.600   0.600], "defaultaxesfontname", "Linux Biolinum") ; clear ax ;
set(gcf, "defaulttextfontname", "Linux Biolinum", "defaultaxesfontname", "Linux Biolinum") ;
set(gcf, "defaultaxesfontsize", 18, "defaulttextfontsize", 18)
ITIM = ismember(CEOF.id(:,1), 1979:2019) & CEOF.IM ;
ILON = ismember(olr.lon, 80:2.5:90) ; ILAT = ismember(olr.lat, 5:2.5:10) ;

clf ; j = 0 ;
ax(++j) = subplot(2,2,j) ;
olr.ref = filtfilt(bw.b, bw.a, squeeze(mean(mean(olr.x(ITIM,ILON,ILAT), 2), 3))) ;
ILAT = ismember(olr.lat, -10:2.5:25) ;
olr.xlat = filtfilt(bw.b, bw.a, squeeze(mean(olr.x(ITIM,ILON,ILAT), 2))) ;
XC = arrayfun(@(l) arrayfun(@(i) corrcoef(shiftg(olr.xlat(:,i), -l), olr.ref(:), "rows", "complete")(2,1), 1:sum(ILAT))', L=-40:40, "UniformOutput", false) ;
XC = cell2mat(XC)' ;
contourf(L, olr.lat(ILAT), XC')
cl(j,:) = get(gca, "clim") ;
title("OBS") ;
ax(++j) = subplot(2,2,j) ;
EOF.xlat = reshape(EOF.pc(:,1:2) * EOF.pat(:,1:2)', [olr.N(1:3) 2]) ;
EOF.ref = filtfilt(bw.b, bw.a, squeeze(mean(mean(EOF.xlat(ITIM,ILON,ILAT), 2), 3))) ;
ILAT = ismember(olr.lat, -10:2.5:25) ;
EOF.xlat = filtfilt(bw.b, bw.a, squeeze(mean(EOF.xlat(ITIM,ILON,ILAT), 2))) ;
XC = arrayfun(@(l) arrayfun(@(i) corrcoef(shiftg(EOF.xlat(:,i), -l), EOF.ref(:), "rows", "complete")(2,1), 1:sum(ILAT))', L=-40:40, "UniformOutput", false) ;
XC = cell2mat(XC)' ;
contourf(L, olr.lat(ILAT), XC')
cl(j,:) = get(gca, "clim") ;
title("EOF") ;
ax(++j) = subplot(2,2,j) ;
CEOF.xlat = reshape(real(CEOF.pc(:,jC) * CEOF.pat(:,jC)'), [olr.N(1:3) 2]) ;
CEOF.ref = filtfilt(bw.b, bw.a, squeeze(mean(mean(CEOF.xlat(ITIM,ILON,ILAT), 2), 3))) ;
ILAT = ismember(olr.lat, -10:2.5:25) ;
CEOF.xlat = filtfilt(bw.b, bw.a, squeeze(mean(CEOF.xlat(ITIM,ILON,ILAT), 2))) ;
XC = arrayfun(@(l) arrayfun(@(i) corrcoef(shiftg(CEOF.xlat(:,i), -l), CEOF.ref(:), "rows", "complete")(2,1), 1:sum(ILAT))', L=-40:40, "UniformOutput", false) ;
XC = cell2mat(XC)' ;
contourf(L, olr.lat(ILAT), XC')
cl(j,:) = get(gca, "clim") ;
title("CEOF") ;
ax(++j) = subplot(2,2,j) ;
CEOF.xlat = reshape(real(CEOF.cc(:,jC) * CEOF.pat(:,jC)'), [olr.N(1:3) 2]) ;
CEOF.ref = filtfilt(bw.b, bw.a, squeeze(mean(mean(CEOF.xlat(ITIM,ILON,ILAT), 2), 3))) ;
ILAT = ismember(olr.lat, -10:2.5:25) ;
CEOF.xlat = filtfilt(bw.b, bw.a, squeeze(mean(CEOF.xlat(ITIM,ILON,ILAT), 2))) ;
XC = arrayfun(@(l) arrayfun(@(i) corrcoef(shiftg(CEOF.xlat(:,i), -l), CEOF.ref(:), "rows", "complete")(2,1), 1:sum(ILAT))', L=-40:40, "UniformOutput", false) ;
XC = cell2mat(XC)' ;
contourf(L, olr.lat(ILAT), XC')
cl(j,:) = get(gca, "clim") ;
title(rCEOFn) ;

set(ax, "clim", [min(cl(:,1)) max(cl(:,2))], "Nextplot", "add", "xlabel", "time lag (d)", "ylabel", "latitude") ;
arrayfun(@(j) colorbar(ax(j)), 1:j) ;
hq = arrayfun(@(j) quiver(ax(j), 10, 0, 10, 10), 1:j) ;
set(hq, "color", "white") ;
colormap gray

print(sprintf("nc/prop.svg")) ;
